package com.liu.mall.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
public class PropertiesUtil {
    private static final Logger logger = LoggerFactory.getLogger(PropertiesUtil.class);
    private static final String PORT;
    private static final String DOMAIN;
    private static final String CALLBACK;

    static {
        Properties properties = new Properties();
        InputStream inputStream = PropertiesUtil.class.getClassLoader().getResourceAsStream("config" +
                ".properties");
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            logger.error("config.properties File not found!");
        }
        PORT = (String) properties.get("port");
        DOMAIN = (String) properties.get("domain");
        CALLBACK = (String) properties.get("callback");
    }


    public static String getPort() {
        return PORT;
    }

    public static String getDomain() {
        return DOMAIN;
    }

    public static String getCallback() {
        return CALLBACK;
    }

    private PropertiesUtil() {
    }
}