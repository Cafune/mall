package com.liu.mall.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

/*
*    账号：hgcrlu6519@sandbox.com
*    密码：111111
*    访问路径：localhost:8081/cart/alipay?cname=qw&totalprice=12
* */

//解决跨域问题
@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)
@Configuration
public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
/*public static String APPID = "2021000121671228";*/
public static String APPID = "2021000121671228";
    // 私钥 pkcs8格式的
    /*public static String RSA_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCjv/PfcTTSRuVjOO/Ou4keHKfXEpymPVALUh+vU5ErirRfgL9DehTN1hCyVnCOMRXXdF62sjaoFvQvp7rSMQGN0HBWZUZvU93p1MZq5nXshy8AUOAgijtX/BrWLlBtq1rUECuTG4ycoP+Z1wqN2Kt/69dLVQjbEGvcfFfVHEt1VdARaGMTOGRVZ3SEbwUnA3bS2kFL10R3YYAgTsD4KrmXP2BJbCJLks/OO6CT+u/UFAAwmjvOeUmlrkAs0M6WPADnAn7kMytxRJiBa31u0auNj2tK3cWEQ40kDOfwWuJ8sQDirLV8Nd5Ilj/BggJRZ6eY9fvo7iqb3xdkiCLZC6J9AgMBAAECggEAetILFVlQCThr7ZRTKni3TAKqUUMAhCAQ3aznXxYLGOGckNV8oBv9ZrZdJ6Z2HfDrNQNv1xCstj8hQG1KT0W6qKfi2qpzviTlmfGiMaRSZuqucko3EbEc+c16Q5PRd7DDG7RVOq27Ne/8R2Nn4yTU3coDXdmWHtmeTW5mYs4qOwLCKy+ACCvjcVi9/7L2CKQJMCVHelMO7L0Y1LQNeynHt5DDFhqSfHOn14kUuFk+H/I08UyKR8n5iyaaFlOJWSMD6F9HseF/0wJ7ux3bl8dZFYKyCJhx7zT2LlH6WQl9FR+k6Km8PXaVpUoDUe/SqkbGtn4+Z+mLxV62B83npHW8cQKBgQDelCYuNCe8O4+BTmktizot4iJ6zt3/9EjYToNQv4KS+LCjgUqOxzmOFicceQdUOfFng3YOF054pqMcP3akSee9IEB0aextLSG/2UaCohNhbxZFsIa00sUmjUbg843ijUsamoZ04cO3R5R11wsnO8UQz7HT1iZ+ow+2KBGz7sDLmwKBgQC8VnDKRw6YqIS7Jpo3ilGOVF0Ti+WTQbn9CEkgBgowvI15RbdqRoJbI0u38qHUrLHEgjht2rm8PfmipCQxMJ8GKNu/L6FfcBmFMmqlvFVgfNzOzRgJTMdN4umQBe0HG8QBnID4LQX/x1NyEbHZDYuAnV52U5u3+gFg/nJ6ZuZnxwKBgQC28Pm4vDF0q1e2JiaYwnidQeKpIeKh/7zn3RBdjCrcdf9pRFltpR4PRPH8mxWFCuL5+7lKt6lNzVj/XMI9ImmQrzOK+4zR52h38TPdJy3axZ1+xirR8PMOTsxVFJon/Y4fXabp5q4gtHq3qHqIUr4UpUE6JigZqhi91Y1HYROhxQKBgEb3r4KZKefeQD2hOyVIipi+M2BlK7nEX4f4Mk3N2NMqiYiz+3whW4dqEmMbG13fQRESHJ9QdwJ2u/VMMpMShim1BbDSCtXCkOs+5y7Se71i73VBlRgUBVnU8juh8AHotYAB8BsUJBmQi2IFRuXKE9H/+pNNx/SYUSKyXoK++X0LAoGBAMk0G21rgOUAH2u1c0/7YlspqJ4LtHp/GBK1quz3lc/5RhoO4ULoA/c+wC54KJ668PAboRER8COw94TPwlnkmn08Q6rvIcSE1Ns1UNOg3TAlRrivqLSnQnK8Bgk1Q68edZTffmcuIE4jPDgu7soECUAc5cYh/UBU595CRL6iPt6j";*/
    public static String RSA_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCFc11gmPVbUHxIsrAJzpPrB15EpzJGBur1LZVx9MoCZmB5TpkDS+s5j4PiKr9PFQcbyuVUDWlmLmYFwzdgOa1MSS0QdcxIDim8jABPxwHpkeI3H8EJfx9vYN4A3asWrct0eYBM+pGP+6UDRLLMJlDzVHvR5+lGIUwucz2+yb27A2peGm/dMqCdDT3eEcQhQST3q6FCiH6vruGmEo2ewhLz7iGGPHU9I9xdEgev5E3AOedPj8aZRo8MKX1T7KkEX2WHKL8Yqm359tJNWX+/Qcxllshvc5XVaEoOCA6f7r0u8Q+dPjvgw/XH2MqrF7gA8TTHEKaW45zzYTAuIbmdNSy1AgMBAAECggEAPRTx354DZ9luGaRYORTp1YDY4BmbRDw0XSaRKci+kdykOcIE/WJJd14+Ik4opJzeVRVIZHGDFbcVEXwqI4wiyzEPRGJ2r2B0MA3wspNHR85zOYH6TEt4avVuh/zuK4I/Br/6PsHN0JaBD/7oy9KZ6NZ5mjlJJ8Vj5jrvPx1+D1O1xcvP4PfkhiCh3M7FWDV7K0mMm7q76mqCAo0jJ54SfkH5gEhkw5RcBC57mUmVKQQCour0XE4TEsufcw7HUDHy/SQP0PsG5N0COldn1bRVqA92pTzvzAZk9Ik98juspz/n0QFpk8aHBkKZRISdWF3eGBZkK+V+x58SHkVAwoh3PQKBgQDNCM6qlsOou4U9ntfF/2cR0qM4Da50RWKI8lI/DP3zdVgpToV2E7Zsxi5G/skMmeMSaC920wUQTevNONCgOHAdoUJ+i5/FhBhiPW1feKnWG+Gat8x0gjQHsVDxZ2/D7RE+ODwXwfBETZR0toBCtGPLLOxu707FWGzSmgA6LeTCDwKBgQCmn2KCKgZtx9VfOo2MtFwXAzs5cl8rIk8KJyqRjms0UtgYM5otJx0ZOAT2Cn3y05CNI/SS3AXYr5/fI/l+S6ez+5WYjhwYLrHR+v/qVIl5+b/6Ei/j5XG/is6HIDpeSSHSHugW/BVXBovsr2xNTtilXg6BFI6qjWUfFqHpVC6Y+wKBgQCZgrqZZn/f2WaNGFydVfgpk/e4hCXV0A1SU3vY0TViDr2PT8cE/QVcs5T6etC0qpMkuPSwlpX3+x9Na/hnvVnUFWTfslJRY/GNvEgjbwhZ0k47tNlTchjamZvs4nD/1INQfnIfp4jo2t0H9YxaZQ1ClcP/2xSzFN2FYOBCrxHTaQKBgQCkksq+m/mrlvRmUbQb5M0ModFuZUwgk4uUEKlYCV5T2Pu9s8I1lOuFzDICPsy1+kdNUNUx9QYFRzpWMPctylYDTJwuiTawn/Zc21jIP+Vlx8vZmPsaUrbuJJDnIGrmU7+HBaXhhe0h/sJX7dC/7v/HHkafMgnT5UKa2UTk7JP13wKBgBrt8hS8kfhAaomxQLPGpeXWKg03loYxn+D+aOYASWNcz85pDVED7PP6h1IlMQDLC6s/DAMtsUOP+KrVRtHcfKD3AyT0rIrtcHkKlOYUIodijD6EeyFXKhVAbJ7+/KECrHhs8LYZck3BGKhNFUCpAI/OdtE5qhtVhxkPJGIurVok";

    // 支付宝公钥
    /*public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4DjuodLs32KHFj0fjaCs/zAYDO+rWEwnom1G0Kaa8JwdjRU0jdAprH7g7RA0mbtWEBp9jIH+HK6UfVftTujOA1wjRYFUo7QcCVB/p3W8SC6lw0HVObEgH4xc/zks3XY8RxtD+mxagTiMfEJm/kutHnlwJm/jVau7V/YkxXXoZddsRcNSdID1J3gWTv7I5hcyWEqXSHO3Ldj+qtt3otIcbQEij5RwyWaGKsXtxuqAS269m01N6dJPprCmopdw1nGqC4emZOfaLA3WujVEE7l8KgIj9OatezyU6Z2B3s4aB/UIDbMoKxCEWmbyliId4NVYRBTwobz3UY0PsSbOpTq5nwIDAQAB";*/
    public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAz3mE9QPB5czslAsUdH9Y67Fpsm1uNppoewcNkXzU8C/MAlWpcQYrcruXmSf9bP3eXY17nYuI8Pl3r+dIt4ivQvkRQi23R878zyRGH8PGxkYJ38NZSqPBcK551OnsUtMz6LL7AykMLEhz+JHFra+weBYZBlrkkPo2Djs1JnluomP2EjFd2rCcGTuXmIfEZgu2T7j8SDfZeXztKnxseea3sex+vkIBp5BRzhI0c6fi0s0L1aF+FDqkFFFYlQuE45aTcW4/ZW0PbIr/q+/6ip5w7UTG7lWhuKxDqV1s++L5iyWVub2EFsaZiPzjt9NyRDG1pvINfn90fSbwEiE3dJQ/1wIDAQAB";


    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //success   http://工程公网访问地址/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp
	public static String notify_url = "http://localhost:8080/#/myOrder";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://localhost:8080/#/myOrder";

	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
    public static String URL = "https://openapi.alipaydev.com/gateway.do";

	// 支付宝网关
	public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

