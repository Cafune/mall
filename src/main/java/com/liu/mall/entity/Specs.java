package com.liu.mall.entity;

public class Specs {
    private int spid;
    private String stype;

    public Specs() {
    }

    public Specs(int spid, String stype) {
        this.spid = spid;
        this.stype = stype;
    }

    public int getSpid() {
        return spid;
    }

    public void setSpid(int spid) {
        this.spid = spid;
    }

    public String getStype() {
        return stype;
    }

    public void setStype(String stype) {
        this.stype = stype;
    }
}
