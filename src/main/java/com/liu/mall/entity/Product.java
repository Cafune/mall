package com.liu.mall.entity;
//商品实体类
public class Product {
    private int pid;//商品id
    private String pname;//商品名字
    private String pprice;//商品价格
    private String ppath;//商品图片
    private String pdescribe;//商品描述
    private String ptype;//商品类型
    private String pbrand;//商品品牌
    private int pstock;//商品库存
    private String psale;//商品是否上架
    private String pstockout;//商品是否缺货

    public Product() {
    }

    public Product(int pid, String pname, String pprice, String ppath, String pdescribe, String ptype, String pbrand, int pstock, String psale, String pstockout) {
        this.pid = pid;
        this.pname = pname;
        this.pprice = pprice;
        this.ppath = ppath;
        this.pdescribe = pdescribe;
        this.ptype = ptype;
        this.pbrand = pbrand;
        this.pstock = pstock;
        this.psale = psale;
        this.pstockout = pstockout;
    }

    public Product(String pname,String ppath,String pdescribe,String ptype,String pbrand,String psale,String pstockout,String pprice,int pstock) {
        this.pname = pname;
        this.pprice = pprice;
        this.ppath = ppath;
        this.pdescribe = pdescribe;
        this.ptype = ptype;
        this.pbrand = pbrand;
        this.pstock = pstock;
        this.psale = psale;
        this.pstockout = pstockout;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPprice() {
        return pprice;
    }

    public void setPprice(String pprice) {
        this.pprice = pprice;
    }

    public String getPpath() {
        return ppath;
    }

    public void setPpath(String ppath) {
        this.ppath = ppath;
    }

    public String getPdescribe() {
        return pdescribe;
    }

    public void setPdescribe(String pdescribe) {
        this.pdescribe = pdescribe;
    }

    public String getPtype() {
        return ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    public String getPbrand() {
        return pbrand;
    }

    public void setPbrand(String pbrand) {
        this.pbrand = pbrand;
    }

    public int getPstock() {
        return pstock;
    }

    public void setPstock(int pstock) {
        this.pstock = pstock;
    }

    public String getPsale() {
        return psale;
    }

    public void setPsale(String psale) {
        this.psale = psale;
    }

    public String getPstockout() {
        return pstockout;
    }

    public void setPstockout(String pstockout) {
        this.pstockout = pstockout;
    }
}
