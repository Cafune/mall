package com.liu.mall.entity;

//订单实体类
public class Order {
    private int orid;//订单id
    private String ono;//订单编号
    private String otime;//下单时间
    private int userid;//用户id
    private String uname;//用户名字
    private String utel;//用户手机号
    private String uadress;//收货地址
    private int pid;//商品id
    private Product product;
    private String pspecs;//商品规格
    private int cnum;//商品数量
    private String ctotal;//商品金额
    private String otype;//支付方式
    private String ostate;//订单状态
    private String returnreason;//退货原因
    private String logistics;//物流公司

    public Order() {
    }

    public Order(String ono, String otime, int userid, String uname, String utel, String uadress, int pid, String pspecs, int cnum, String ctotal, String otype, String ostate, String returnreason) {
        this.ono = ono;
        this.otime = otime;
        this.userid = userid;
        this.uname = uname;
        this.utel = utel;
        this.uadress = uadress;
        this.pid = pid;
        this.pspecs = pspecs;
        this.cnum = cnum;
        this.ctotal = ctotal;
        this.otype = otype;
        this.ostate = ostate;
        this.returnreason = returnreason;
    }

    public Order(int orid, String ono, String otime, int userid, String uname, String utel, String uadress, int pid, Product product, String pspecs, int cnum, String ctotal, String otype, String ostate, String returnreason,String logistics) {
        this.orid = orid;
        this.ono = ono;
        this.otime = otime;
        this.userid = userid;
        this.uname = uname;
        this.utel = utel;
        this.uadress = uadress;
        this.pid = pid;
        this.product = product;
        this.pspecs = pspecs;
        this.cnum = cnum;
        this.ctotal = ctotal;
        this.otype = otype;
        this.ostate = ostate;
        this.returnreason = returnreason;
        this.logistics=logistics;
    }

    public String getLogistics() {
        return logistics;
    }

    public void setLogistics(String logistics) {
        this.logistics = logistics;
    }

    public int getOrid() {
        return orid;
    }

    public void setOrid(int orid) {
        this.orid = orid;
    }

    public String getOno() {
        return ono;
    }

    public void setOno(String ono) {
        this.ono = ono;
    }

    public String getOtime() {
        return otime;
    }

    public void setOtime(String  otime) {
        this.otime = otime;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUtel() {
        return utel;
    }

    public void setUtel(String utel) {
        this.utel = utel;
    }

    public String getUadress() {
        return uadress;
    }

    public void setUadress(String uadress) {
        this.uadress = uadress;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getPspecs() {
        return pspecs;
    }

    public void setPspecs(String pspecs) {
        this.pspecs = pspecs;
    }

    public int getCnum() {
        return cnum;
    }

    public void setCnum(int cnum) {
        this.cnum = cnum;
    }

    public String getCtotal() {
        return ctotal;
    }

    public void setCtotal(String ctotal) {
        this.ctotal = ctotal;
    }

    public String getOtype() {
        return otype;
    }

    public void setOtype(String otype) {
        this.otype = otype;
    }

    public String getOstate() {
        return ostate;
    }

    public void setOstate(String ostate) {
        this.ostate = ostate;
    }

    public String getReturnreason() {
        return returnreason;
    }

    public void setReturnreason(String returnreason) {
        this.returnreason = returnreason;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
