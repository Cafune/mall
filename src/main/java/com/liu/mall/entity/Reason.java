package com.liu.mall.entity;

public class Reason {
    private int reid;
    private String rreason;
    private String rstate;

    public Reason() {
    }

    public Reason(String rreason) {
        this.rreason = rreason;
    }

    public Reason(int reid, String rreason, String rstate) {
        this.reid = reid;
        this.rreason = rreason;
        this.rstate = rstate;
    }

    public int getReid() {
        return reid;
    }

    public void setReid(int reid) {
        this.reid = reid;
    }

    public String getRreason() {
        return rreason;
    }

    public void setRreason(String rreason) {
        this.rreason = rreason;
    }

    public String getRstate() {
        return rstate;
    }

    public void setRstate(String rstate) {
        this.rstate = rstate;
    }
}
