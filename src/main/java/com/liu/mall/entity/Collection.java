package com.liu.mall.entity;

public class Collection {
    private int coid;
    private int pid;
    private int userid;
    private Product product;

    public Collection() {
    }

    public Collection(int pid, int userid) {
        this.pid = pid;
        this.userid = userid;
    }

    public Collection(int coid, int pid, int userid, Product product) {
        this.coid = coid;
        this.pid = pid;
        this.userid = userid;
        this.product = product;
    }

    public int getCoid() {
        return coid;
    }

    public void setCoid(int coid) {
        this.coid = coid;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
