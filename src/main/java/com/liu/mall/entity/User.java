package com.liu.mall.entity;
//用户实体类
public class User {
    private int userid;//用户id
    private String uname;//用户名字
    private String upwd;//用户密码
    private int utype;//用户类型（1：管理员且唯一，2：普通用户）
    private String usex;//用户性别
    private String utel;//用户手机号
    private String uadress;//用户地址（即收货地址）
    private String upath;//用户头像
    private String rtime;

    public User() {
    }

    public User(String uname, String upwd,String rtime) {

        this.uname = uname;
        this.upwd = upwd;
        this.rtime=rtime;
    }

    public User(int userid, String uname, String upwd, int utype, String usex, String utel, String uadress, String upath,String rtime) {
        this.userid = userid;
        this.uname = uname;
        this.upwd = upwd;
        this.utype = utype;
        this.usex = usex;
        this.utel = utel;
        this.uadress = uadress;
        this.upath = upath;
        this.rtime=rtime;
    }

    public User(int userid, String uname, String upwd, int utype, String usex, String utel, String uadress, String upath) {
        this.userid = userid;
        this.uname = uname;
        this.upwd = upwd;
        this.utype = utype;
        this.usex = usex;
        this.utel = utel;
        this.uadress = uadress;
        this.upath = upath;
    }

    public User(int userid, String uname, String usex, String utel, String uadress, String upath) {
        this.userid = userid;
        this.uname = uname;
        this.usex = usex;
        this.utel = utel;
        this.uadress = uadress;
        this.upath = upath;
    }

    public String getRtime() {
        return rtime;
    }

    public void setRtime(String rtime) {
        this.rtime = rtime;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUpwd() {
        return upwd;
    }

    public void setUpwd(String upwd) {
        this.upwd = upwd;
    }

    public int getUtype() {
        return utype;
    }

    public void setUtype(int utype) {
        this.utype = utype;
    }

    public String getUsex() {
        return usex;
    }

    public void setUsex(String usex) {
        this.usex = usex;
    }

    public String getUtel() {
        return utel;
    }

    public void setUtel(String utel) {
        this.utel = utel;
    }

    public String getUadress() {
        return uadress;
    }

    public void setUadress(String uadress) {
        this.uadress = uadress;
    }

    public String getUpath() {
        return upath;
    }

    public void setUpath(String upath) {
        this.upath = upath;
    }
}
