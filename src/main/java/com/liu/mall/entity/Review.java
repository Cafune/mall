package com.liu.mall.entity;

//评价实体类
public class Review {
    private int rid;//评论id
    private int userid;
    private int pid;
    private  String pspecs;
    private String ono;
    private String rtime;
    private int starlevel;
    private String preview;
    private User user;
    private Product product;

    public Review() {
    }

    public Review(int rid, int userid, int pid, String pspecs, String ono, String rtime, int starlevel, String preview) {
        this.rid = rid;
        this.userid = userid;
        this.pid = pid;
        this.pspecs = pspecs;
        this.ono = ono;
        this.rtime = rtime;
        this.starlevel = starlevel;
        this.preview = preview;
    }

    public Review(int userid, int pid, String pspecs, String ono, String rtime, int starlevel, String preview) {
        this.userid = userid;
        this.pid = pid;
        this.pspecs = pspecs;
        this.ono = ono;
        this.rtime = rtime;
        this.starlevel = starlevel;
        this.preview = preview;
    }

    public Review(int rid, int userid, int pid, String pspecs, String ono, String rtime, int starlevel, String preview, User user) {
        this.rid = rid;
        this.userid = userid;
        this.pid = pid;
        this.pspecs = pspecs;
        this.ono = ono;
        this.rtime = rtime;
        this.starlevel = starlevel;
        this.preview = preview;
        this.user = user;
    }

    public Review(int rid, int userid, int pid, String pspecs, String ono, String rtime, int starlevel, String preview, User user, Product product) {
        this.rid = rid;
        this.userid = userid;
        this.pid = pid;
        this.pspecs = pspecs;
        this.ono = ono;
        this.rtime = rtime;
        this.starlevel = starlevel;
        this.preview = preview;
        this.user = user;
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getPspecs() {
        return pspecs;
    }

    public void setPspecs(String pspecs) {
        this.pspecs = pspecs;
    }

    public String getOno() {
        return ono;
    }

    public void setOno(String ono) {
        this.ono = ono;
    }

    public String getRtime() {
        return rtime;
    }

    public void setRtime(String rtime) {
        this.rtime = rtime;
    }

    public int getStarlevel() {
        return starlevel;
    }

    public void setStarlevel(int starlevel) {
        this.starlevel = starlevel;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }
}
