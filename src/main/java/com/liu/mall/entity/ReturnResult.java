package com.liu.mall.entity;

public class ReturnResult {
    private int rsid;
    private int orid;
    private String ono;
    private int userid;
    private String uname;
    private String aname;
    private String applytime;
    private String returntime;
    private String reason;
    private String rstate;
    private String ctotal;


    public ReturnResult() {
    }

    public ReturnResult(int rsid, int orid, String ono, int userid, String uname, String aname, String applytime, String returntime, String reason, String rstate,String ctotal) {
        this.rsid = rsid;
        this.orid = orid;
        this.ono = ono;
        this.userid = userid;
        this.uname = uname;
        this.aname = aname;
        this.applytime = applytime;
        this.returntime = returntime;
        this.reason = reason;
        this.rstate = rstate;
        this.ctotal=ctotal;

    }

    public ReturnResult(int orid, String ono, int userid, String uname, String aname, String applytime, String returntime, String reason, String rstate,String ctotal) {
        this.orid = orid;
        this.ono = ono;
        this.userid = userid;
        this.uname = uname;
        this.aname = aname;
        this.applytime = applytime;
        this.returntime = returntime;
        this.reason = reason;
        this.rstate = rstate;
        this.ctotal=ctotal;

    }



    public int getRsid() {
        return rsid;
    }

    public void setRsid(int rsid) {
        this.rsid = rsid;
    }

    public int getOrid() {
        return orid;
    }

    public void setOrid(int orid) {
        this.orid = orid;
    }

    public String getOno() {
        return ono;
    }

    public void setOno(String ono) {
        this.ono = ono;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getApplytime() {
        return applytime;
    }

    public void setApplytime(String applytime) {
        this.applytime = applytime;
    }

    public String getReturntime() {
        return returntime;
    }

    public void setReturntime(String returntime) {
        this.returntime = returntime;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getRstate() {
        return rstate;
    }

    public void setRstate(String rstate) {
        this.rstate = rstate;
    }

    public String getCtotal() {
        return ctotal;
    }

    public void setCtotal(String ctotal) {
        this.ctotal = ctotal;
    }
}
