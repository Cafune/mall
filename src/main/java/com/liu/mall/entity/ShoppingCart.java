package com.liu.mall.entity;

public class ShoppingCart {
    private int cid;
    private int userid;
    private Product product;
    private int pid;
    private int cnum;
    private String ctotal;
    private String cspecs;

    public ShoppingCart() {
    }

    public ShoppingCart(int cid, int userid, Product product, int pid, int cnum, String ctotal, String cspecs) {
        this.cid = cid;
        this.userid = userid;
        this.product = product;
        this.pid = pid;
        this.cnum = cnum;
        this.ctotal = ctotal;
        this.cspecs = cspecs;
    }

    public ShoppingCart(int cid, int userid, int pid, int cnum, String ctotal, String cspecs) {
        this.cid = cid;
        this.userid = userid;
        this.pid = pid;
        this.cnum = cnum;
        this.ctotal = ctotal;
        this.cspecs = cspecs;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getCnum() {
        return cnum;
    }

    public void setCnum(int cnum) {
        this.cnum = cnum;
    }

    public String getCtotal() {
        return ctotal;
    }

    public void setCtotal(String ctotal) {
        this.ctotal = ctotal;
    }

    public String getCspecs() {
        return cspecs;
    }

    public void setCspecs(String cspecs) {
        this.cspecs = cspecs;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
