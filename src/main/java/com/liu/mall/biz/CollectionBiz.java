package com.liu.mall.biz;

import com.liu.mall.entity.Collection;
import com.liu.mall.mapper.CollectionMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface CollectionBiz {
    //加入收藏
    public boolean addCollection(Collection collection);
    //查看我的收藏
    public List<Collection> findMy(int userid);
    //是否收藏
    public Collection isCollection(int userid,int pid);
    //取消收藏
    public boolean delCollection(int userid,int pid);

}
