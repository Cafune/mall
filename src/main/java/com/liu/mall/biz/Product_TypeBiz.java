package com.liu.mall.biz;

import com.liu.mall.entity.Product_Type;

import java.util.List;

public interface Product_TypeBiz {
    public List<Product_Type> findName();
}
