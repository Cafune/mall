package com.liu.mall.biz.impl;

import com.liu.mall.biz.ReasonBiz;
import com.liu.mall.entity.Reason;
import com.liu.mall.mapper.ReasonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("reasonBiz")
public class ReasonBizImpl implements ReasonBiz {
    @Autowired
    private ReasonMapper reasonMapper;

    @Override
    public List<Reason> findReason() {
        return reasonMapper.findReason();
    }

    @Override
    public boolean updateReason(int reid, String rreason) {
        return reasonMapper.updateReason(reid,rreason);
    }

    @Override
    public boolean delReason(int reid) {
        return reasonMapper.delReason(reid);
    }

    @Override
    public boolean addReason(Reason reason) {
        return reasonMapper.addReason(reason);
    }

    @Override
    public List<Reason> findReason1(String rreason) {
        return reasonMapper.findReason1(rreason);
    }
}
