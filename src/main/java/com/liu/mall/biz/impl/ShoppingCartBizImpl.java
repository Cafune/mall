package com.liu.mall.biz.impl;

import com.liu.mall.biz.ShoppingCartBiz;
import com.liu.mall.entity.ShoppingCart;
import com.liu.mall.mapper.ShoppingCartMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("shoppingcartBiz")
public class ShoppingCartBizImpl implements ShoppingCartBiz {
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    @Override
    public List<ShoppingCart> findCart(int userid) {
        return shoppingCartMapper.findCart(userid);
    }

    @Override
    public boolean addCart(ShoppingCart shoppingCart) {
         boolean bo=shoppingCartMapper.addCart(shoppingCart);
        if (bo){
            return true;
        }else{
            return  false;
        }
    }

    @Override
    public boolean delCart(int cid) {
        boolean bo=shoppingCartMapper.delCart(cid);
        if (bo){
            return true;
        }else{
            return  false;
        }
    }
}
