package com.liu.mall.biz.impl;

import com.liu.mall.biz.Product_BrandBiz;
import com.liu.mall.entity.Product_Brand;
import com.liu.mall.mapper.Product_BrandMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("product_brandBiz")
public class Product_BrandBizImpl implements Product_BrandBiz {
    @Autowired
    private Product_BrandMapper product_brandMapper;

    @Override
    public List<Product_Brand> findName() {
        return product_brandMapper.findName();
    }
}
