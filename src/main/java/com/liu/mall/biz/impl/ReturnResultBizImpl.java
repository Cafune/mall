package com.liu.mall.biz.impl;

import com.liu.mall.biz.ReturnResultBiz;
import com.liu.mall.entity.ReturnResult;
import com.liu.mall.mapper.ReturnResultMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("returnresultBiz")
public class ReturnResultBizImpl implements ReturnResultBiz {
    @Autowired
    private ReturnResultMapper returnResultMapper;

    @Override
    public boolean addReturn(ReturnResult returnResult) {
        return returnResultMapper.addReturn(returnResult);
    }

    @Override
    public List<ReturnResult> findAll() {
        return returnResultMapper.findAll();
    }

    @Override
    public boolean isReturn(int rsid, String aname, String returntime) {
        return returnResultMapper.isReturn(rsid,aname,returntime);
    }
}
