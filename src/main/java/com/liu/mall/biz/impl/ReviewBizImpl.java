package com.liu.mall.biz.impl;

import com.liu.mall.biz.ReviewBiz;
import com.liu.mall.entity.Review;
import com.liu.mall.mapper.ReviewMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("reviewBiz")
public class ReviewBizImpl implements ReviewBiz {
    @Autowired
    private ReviewMapper reviewMapper;

    @Override
    public boolean addReview(Review review) {
        boolean bo=reviewMapper.addReview(review);
        if(bo){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public List<Review> findReview(int pid) {
        return reviewMapper.findReview(pid);
    }

    public List<Review> findMy(int userid){
        return  reviewMapper.findMy(userid);
    }
}
