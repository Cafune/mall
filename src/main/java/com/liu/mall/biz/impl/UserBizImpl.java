package com.liu.mall.biz.impl;

import com.liu.mall.biz.UserBiz;
import com.liu.mall.entity.User;
import com.liu.mall.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userBiz")
public class UserBizImpl implements UserBiz {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User login(String uname,String utel, String upwd) {
        return userMapper.login(uname,utel,upwd);
    }

    @Override
    public boolean register(User user) {
        return userMapper.register(user);
    }

    @Override
    public User ExitsName(String uname) {
        return userMapper.ExitsName(uname);
    }

    @Override
    public User findId(int userid) {
        return userMapper.findId(userid);
    }

    @Override
    public boolean updateadress(int userid,String uadress) {
        return userMapper.updateadress(userid,uadress);
    }

    @Override
    public boolean updateUser(User user) {
        return userMapper.updateUser(user);
    }

    @Override
    public boolean updatePassword(int userid,String upwd) {
        return userMapper.updatePassword(userid,upwd);
    }

    @Override
    public List<User> findUser() {
        return userMapper.findUser();
    }

    @Override
    public List<User> found(String uname,String utel) {
        return userMapper.found(uname,utel);
    }
}
