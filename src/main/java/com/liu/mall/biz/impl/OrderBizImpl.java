package com.liu.mall.biz.impl;

import com.liu.mall.biz.OrderBiz;
import com.liu.mall.entity.Order;
import com.liu.mall.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("orderBiz")
public class OrderBizImpl implements OrderBiz {
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public List<Order> findMy(int userid) {

        return orderMapper.findMy(userid);
    }

    @Override
    public Order findId(int orid) {
        return orderMapper.findId(orid);
    }

    @Override
    public List<Order> findOrder() {
        return orderMapper.findOrder();
    }

    @Override
    public boolean jiesuan(int orid) {
        return orderMapper.jiesuan(orid);
    }

    @Override
    public boolean updatelogistics(int orid, String logistics) {
        return orderMapper.updatelogistics(orid,logistics);
    }

    @Override
    public boolean updateReturn(int orid,String returnreason) {
        return orderMapper.updateReturn(orid,returnreason);
    }

    @Override
    public boolean updateReturn1(int orid) {
        return orderMapper.updateReturn1(orid);
    }

    @Override
    public boolean returnmoney(int orid) {
        return orderMapper.returnmoney(orid);
    }

    @Override
    public boolean updateFinishReturn(int orid) {
        return orderMapper.updateFinishReturn(orid);
    }

    @Override
    public boolean addOrder(Order order) {
        boolean bo=orderMapper.addOrder(order);
        if(bo){
            return  true;
        }else {
            return false;
        }
    }

    @Override
    public List<Order> findNopay(int userid) {
        return orderMapper.findNopay(userid);
    }

    @Override
    public List<Order> findNoSend(int userid) {
        return orderMapper. findNoSend(userid);
    }

    @Override
    public boolean finishOrder(String ono) {
        return orderMapper.finishOrder(ono);
    }

    @Override
    public boolean findConfirm(int orid) {
        return orderMapper.findConfirm(orid);
    }

    @Override
    public boolean findConfirmSend(int orid) {
        return orderMapper.findConfirmSend(orid);
    }

    @Override
    public List<Order> findSend(int userid) {
        return orderMapper. findSend(userid);
    }

    @Override
    public boolean delOrder(int orid) {
        boolean bo=orderMapper.delOrder(orid);
        if (bo){
            return true;
        }else{
            return  false;
        }
    }

    @Override
    public int findCount() {
        return orderMapper.findCount();
    }

    @Override
    public int findNoPayCount() {
        return orderMapper.findNoPayCount();
    }

    @Override
    public int findDealCount() {
        return orderMapper.findDealCount();
    }

    @Override
    public int findFinishCount() {
        return orderMapper.findFinishCount();
    }

    @Override
    public int findNoDealCount() {
        return orderMapper.findNoDealCount();
    }

    @Override
    public int findNoSendCount() {
        return orderMapper.findNoSendCount();
    }

    @Override
    public int findSendCount() {
        return orderMapper.findSendCount();
    }

    @Override
    public int findWaitSendCount() {
        return orderMapper.findWaitSendCount();
    }

    @Override
    public List<Order> findtui() {
        return orderMapper.findtui();
    }

    @Override
    public List<Order> found(String ono, String uname, String ostate, String otype) {
        return orderMapper.found(ono,uname,ostate,otype);
    }

    @Override
    public int isbuy(int userid, int pid) {
        return orderMapper.isbuy(userid,pid);
    }
}
