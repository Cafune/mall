package com.liu.mall.biz.impl;

import com.liu.mall.biz.Product_TypeBiz;
import com.liu.mall.entity.Product_Type;
import com.liu.mall.mapper.Product_TypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("peoduct_typeBiz")
public class Product_TypeBizImpl implements Product_TypeBiz {
    @Autowired
    private Product_TypeMapper product_typeMapper;

    @Override
    public List<Product_Type> findName() {

        return product_typeMapper.findName();
    }
}
