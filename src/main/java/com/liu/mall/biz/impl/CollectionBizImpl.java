package com.liu.mall.biz.impl;

import com.liu.mall.biz.CollectionBiz;
import com.liu.mall.entity.Collection;
import com.liu.mall.mapper.CollectionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("collectionBiz")
public class CollectionBizImpl implements CollectionBiz {
    @Autowired
    private CollectionMapper collectionMapper;

    @Override
    public boolean addCollection(Collection collection) {
        return collectionMapper.addCollection(collection);
    }

    @Override
    public List<Collection> findMy(int userid) {
        return collectionMapper.findMy(userid);
    }

    @Override
    public Collection isCollection(int userid, int pid) {
        return collectionMapper.isCollection(userid,pid);
    }

    @Override
    public boolean delCollection(int userid, int pid) {
        return collectionMapper.delCollection(userid,pid);
    }
}
