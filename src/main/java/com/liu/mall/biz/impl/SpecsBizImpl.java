package com.liu.mall.biz.impl;

import com.liu.mall.biz.SpecsBiz;
import com.liu.mall.entity.Specs;
import com.liu.mall.mapper.SpecsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("specsBiz")
public class SpecsBizImpl implements SpecsBiz {
    @Autowired
    private SpecsMapper specsMapper;

    @Override
    public List<Specs> findAll() {
        return specsMapper.findAll();
    }


}
