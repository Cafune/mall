package com.liu.mall.biz.impl;

import com.liu.mall.biz.ProductBiz;
import com.liu.mall.entity.Product;
import com.liu.mall.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("productBiz")
public class ProductBizImpl implements ProductBiz {
    @Autowired
    private ProductMapper productMapper;

    @Override
    public List<Product> findAllProduct() {

        return productMapper.findAllProduct();
    }

    @Override
    public List<Product> findType(String ptype) {
        return productMapper.findType(ptype);
    }

    @Override
    public List<Product> findBrand(String pbrand) {
        return productMapper.findBrand(pbrand);
    }

    @Override
    public List<Product> findName(String pname,String ptype) {
        return productMapper.findName(pname,ptype);
    }

    @Override
    public List<Product> findNew(int index, int size) {
        Map<String,Object> map=new HashMap<>();
        map.put("index",index);
        map.put("size",size);
        return productMapper.findNew(map);
    }

    @Override
    public Product findId(int pid) {
        return productMapper.findId(pid);
    }

    @Override
    public boolean updateStock(int pstock, int pid) {
        boolean bo=productMapper.updateStock(pstock,pid);
        if (bo){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean updateProduct(Product product) {
        boolean bo=productMapper.updateProduct(product);
        if (bo){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean delProduct(int pid) {
         boolean bo= productMapper.delProduct(pid);
        if (bo){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean addProduct(Product product) {
        boolean bo=productMapper.addProduct(product);
        if (bo){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int productCount() {
        return productMapper.productCount();
    }
}
