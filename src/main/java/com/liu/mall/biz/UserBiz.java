package com.liu.mall.biz;

import com.liu.mall.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserBiz {
    public User login(String uname, String utel,String upwd);
   public boolean register(User user);
    //是否同名
    public User ExitsName(String uname);
    public User findId(int userid);
    //修改地址
    public boolean updateadress( int userid, String uadress);
    //修改用户
    public boolean updateUser(User user);
   //修改密码
    public boolean updatePassword(int userid,String upwd);
    public List<User> findUser();
    public List<User> found(String uname,String utel);
}
