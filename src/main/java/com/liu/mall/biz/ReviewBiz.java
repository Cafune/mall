package com.liu.mall.biz;

import com.liu.mall.entity.Review;

import java.util.List;

public interface ReviewBiz {
    public boolean addReview(Review review);
    public List<Review> findReview(int pid);
    public List<Review> findMy(int userid);
}
