package com.liu.mall.biz;

import com.liu.mall.entity.ShoppingCart;

import java.util.List;

public interface ShoppingCartBiz {
    //查找所有购物车
    public List<ShoppingCart> findCart(int userid);
    //增加购物车信息
    public boolean addCart(ShoppingCart shoppingCart);
    //删除购物车信息
    public boolean delCart(int cid);
}
