package com.liu.mall.biz;

import com.liu.mall.entity.Specs;

import java.util.List;

public interface SpecsBiz {
    public List<Specs> findAll();

}
