package com.liu.mall.biz;

import com.liu.mall.entity.Product_Brand;
import com.liu.mall.entity.Product_Type;

import java.util.List;

public interface Product_BrandBiz {
    public List<Product_Brand> findName();
}
