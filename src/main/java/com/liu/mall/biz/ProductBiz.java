package com.liu.mall.biz;

import com.liu.mall.entity.Product;

import java.util.List;

public interface ProductBiz {
    public List<Product> findAllProduct();
    public List<Product> findType(String ptype);
    //查询品牌
    public List<Product> findBrand(String pbrand);
    public List<Product> findName(String pname,String ptype);
    public List<Product> findNew(int index,int size);
    public Product findId(int pid);
    public boolean updateStock(int pstock,int pid);
    //编辑商品
    public boolean updateProduct(Product product);
    //删除商品
    public boolean delProduct(int pid);
    //添加商品
    public boolean addProduct(Product product);

    public int productCount();

}
