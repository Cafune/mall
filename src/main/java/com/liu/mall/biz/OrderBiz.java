package com.liu.mall.biz;

import com.liu.mall.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderBiz {
    //添加订单
    public boolean addOrder(Order order);
    //查找我的订单
    public List<Order> findMy(int userid);
    //查找订单id
    public Order findId(int orid);
    //结算的方法
    public boolean jiesuan(int orid);
    //查找所有订单
    public List<Order> findOrder();
    //查找未付款的订单
    public List<Order> findNopay(int userid);
    //查找待发货的订单
    public List<Order> findNoSend(int userid);
    //查找待收货的订单
    public List<Order> findSend(int userid);
    //修改物流公司
    public boolean updatelogistics(int orid,String logistics);

    //待退货
    public boolean updateReturn(int orid,String returnreason);

    //待退款
    public boolean updateReturn1(int orid);

    //已退款
    public boolean  returnmoney( int orid);

    //已退货
    public boolean updateFinishReturn(int orid);

    //完成评价修改订单状态
    public boolean finishOrder(String ono);

    //确认收货
    public boolean findConfirm(int orid);
    //确认发货
    public boolean findConfirmSend(int orid);
    //取消订单
    public boolean delOrder(int orid);

    //查找全部订单个数
    public int findCount();
    //查找所有未支付订单个数
    public int findNoPayCount();
    //查找所有待发货订单个数
    public int findNoSendCount();
    //查找所有已发货订单个数
    public int findSendCount();
    //查找所有待确认收获订单个数
    public int findWaitSendCount();
    //查找所有已完成订单个数
    public int findFinishCount();
    //查找所有待退货订单个数
    public int findNoDealCount();
    //查找所有已退款订单个数
    public int findDealCount();
    public List<Order> found(String ono,String uname,String ostate,String otype);

    public List<Order> findtui();

    public int isbuy(int userid,int pid);
}
