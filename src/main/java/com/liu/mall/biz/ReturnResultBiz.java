package com.liu.mall.biz;

import com.liu.mall.entity.ReturnResult;

import java.util.List;

public interface ReturnResultBiz {
    public boolean addReturn(ReturnResult returnResult);
    public List<ReturnResult> findAll();
    public boolean isReturn(int rsid,String aname,String returntime);
}
