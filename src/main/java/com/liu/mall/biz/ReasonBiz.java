package com.liu.mall.biz;

import com.liu.mall.entity.Reason;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ReasonBiz {
    public List<Reason> findReason();
    public boolean updateReason(int reid, String rreason);
    public boolean delReason(int reid);
    public boolean addReason(Reason reason);
    public  List<Reason> findReason1(String rreason);
}
