package com.liu.mall.controller;

import com.liu.mall.biz.CollectionBiz;
import com.liu.mall.entity.Collection;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("collection")
@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)
public class CollectionController {
    @Autowired
    private CollectionBiz collectionBiz;

    //加入收藏
    @RequestMapping(value = "/addCollection")
    @ResponseBody
    public boolean addCollection(int userid,int pid){
        Collection collection1=new Collection(pid,userid);
        return collectionBiz.addCollection(collection1);
    }
    //查看我的收藏
    @RequestMapping(value = "/findMy")
    @ResponseBody
    public List<Collection> findMy(int userid){
        return  collectionBiz.findMy(userid);
    }
    //是否收藏
    @RequestMapping(value = "/isCollection")
    @ResponseBody
    public boolean isCollection(int userid,int pid){
        Collection collection=collectionBiz.isCollection(userid,pid);
        if (collection!=null){
            return  true;
        }else{
            return false;
        }
    }
    //取消收藏
    @RequestMapping(value = "/delCollection")
    @ResponseBody
    public boolean delCollection(int userid,int pid){
        return collectionBiz.delCollection(userid,pid);
    }
}
