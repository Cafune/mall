package com.liu.mall.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * @author:lr
 * @date:2022/8/2316:42
 * @vertion 1.0
 */
@Controller
@RequestMapping(value = "upload")
public class UploadController {
    @RequestMapping(value = "/upload")
    @ResponseBody
    public String upload(MultipartFile file, HttpServletRequest request) throws IOException {
        String fileName="";
        if (!file.isEmpty()) {
            fileName=file.getOriginalFilename();
            String path=request.getServletContext().getRealPath("E:/Reasoure/Fronts/liurong/src/assets/img");
            File file1=new File(path,fileName);
            //将文件上传到磁盘中
            file.transferTo(file1);
           
        }
        return fileName;
    }
}
