package com.liu.mall.controller;

import com.liu.mall.biz.Product_BrandBiz;
import com.liu.mall.entity.Product_Brand;
import com.liu.mall.entity.Product_Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("product_brand")
@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)
public class Product_BrandController {
    @Autowired
    private Product_BrandBiz product_brandBiz;
    @RequestMapping(value = "/findName")
    @ResponseBody
    public List<String> findName(){
        List<Product_Brand> list=product_brandBiz.findName();
        List<String> list1=new ArrayList<>();
        for (int i=0;i<list.size();i++){
            list1.add(list.get(i).getBrand_name());
        }
        return list1;
    }
}
