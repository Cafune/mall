package com.liu.mall.controller;

import com.liu.mall.biz.ReturnResultBiz;
import com.liu.mall.entity.ReturnResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("returnresult")
@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)
public class ReturnResultController {

    Date date=new Date();
    SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private ReturnResultBiz returnResultBiz;

    @RequestMapping(value = "/addReturn")
    @ResponseBody
    public boolean addReturn(int orid, String ono, int userid, String uname, String reason,String ctotal){
       ReturnResult returnResult=new ReturnResult(orid,ono,userid,uname,"",format.format(date),"",reason,"待退货",ctotal);
       return returnResultBiz.addReturn(returnResult);
    }

    @RequestMapping(value = "/findAll")
    @ResponseBody
    public List<ReturnResult> findAll(){
        return returnResultBiz.findAll();
    }

    @RequestMapping(value = "/isReturn")
    @ResponseBody
    public boolean isReturn(int rsid,String aname){
        return returnResultBiz.isReturn(rsid,aname,format.format(date));
    }
}
