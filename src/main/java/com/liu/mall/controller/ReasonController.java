package com.liu.mall.controller;

import com.liu.mall.biz.ReasonBiz;
import com.liu.mall.entity.Reason;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("reason")
@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)
public class ReasonController {
    @Autowired
    private ReasonBiz reasonBiz;

    @RequestMapping(value = "/findReason")
    @ResponseBody
    public List<String> findReason(){
        List<Reason> list=reasonBiz.findReason();
        List<String> list1=new ArrayList<>();
        for (int i=0;i<list.size();i++){
            list1.add(list.get(i).getRreason());
        }
        return list1;
    }

    @RequestMapping(value = "/findAll")
    @ResponseBody
    public List<Reason> findAll(){
        List<Reason> list=reasonBiz.findReason();
        return list;
    }

    @RequestMapping(value = "/updateReason")
    @ResponseBody
    public boolean updateReason(int reid,String rreason){
        return reasonBiz.updateReason(reid,rreason);
    }

    @RequestMapping(value = "/delReason")
    @ResponseBody
    public boolean delReason(int reid){
        return reasonBiz.delReason(reid);
    }

    @RequestMapping(value = "/addReason")
    @ResponseBody
    public boolean addReason(String rreason){
        Reason reason=new Reason(rreason);
        return reasonBiz.addReason(reason);
    }

    @RequestMapping(value = "/findReason1")
    @ResponseBody
    public List<Reason> findReason1(String rreason){
        return reasonBiz.findReason1(rreason);
    }
}
