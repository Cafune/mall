package com.liu.mall.controller;

import com.liu.mall.biz.ProductBiz;
import com.liu.mall.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("product")
@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)
public class ProductController {
    @Autowired
    private ProductBiz productBiz;

    @RequestMapping(value = "/findAllProduct")
    @ResponseBody
    public List<Product> findAllProduct(){
        return productBiz.findAllProduct();
    }
    @RequestMapping(value = "/findType")
    @ResponseBody
    public List<Product> findType(String ptype){
        return productBiz.findType(ptype);
    }

    @RequestMapping(value = "/findBrand")
    @ResponseBody
    public List<Product> findBrand(String pbrand){
        return productBiz.findBrand(pbrand);
    }

    @RequestMapping(value = "/findName")
    @ResponseBody
    public List<Product> findName(String pname,String ptype){
        return productBiz.findName(pname,ptype);
    }

    @RequestMapping(value = "/findNew")
    @ResponseBody
    public List<Product> findNew(Integer index){
        if (index==null){
            index=1;
        }
        int size=5;
        List<Product> list=productBiz.findNew(index,size);
        return list;
    }

    @RequestMapping(value = "/findId")
    @ResponseBody
    public Product findId(int pid){

        return productBiz.findId(pid);
    }

    @RequestMapping(value = "/updateStock")
    @ResponseBody
    public boolean updateStock(int cnum, int pid){
        Product product=productBiz.findId(pid);
        int pstock=product.getPstock()-cnum;
        return productBiz.updateStock(pstock,pid);
    }

    @RequestMapping(value = "/updateStock1")
    @ResponseBody
    public boolean updateStock1(int cnum, int pid){
        Product product=productBiz.findId(pid);
        int pstock=product.getPstock()+cnum;
        return productBiz.updateStock(pstock,pid);
    }

    @RequestMapping(value = "/updateProduct")
    @ResponseBody
    public boolean updateProduct(int pid, String pname, String pprice, String ppath, String pdescribe, String ptype, String pbrand, int pstock){
        Product product=new Product(pid,pname,pprice,ppath,pdescribe,ptype,pbrand,pstock,"上架","否");
        return productBiz.updateProduct(product);
    }

    @RequestMapping(value = "/delProduct")
    @ResponseBody
    public boolean delProduct(Integer pid){
        return productBiz.delProduct(pid);
    }

    @RequestMapping(value = "/addProduct")
    @ResponseBody
    public boolean addProduct(String pname,String ppath,String pdescribe,String ptype,String pbrand,String psale,String pstockout,String pprice,int pstock){
    Product product=new Product(pname,ppath,pdescribe,ptype,pbrand,psale,pstockout,pprice,pstock);
    return productBiz.addProduct(product);
    }

    @RequestMapping(value = "/productCount")
    @ResponseBody
    public int productCount(){
        return  productBiz.productCount();
    }
}
