package com.liu.mall.controller;

import com.liu.mall.biz.UserBiz;
import com.liu.mall.entity.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("user")
@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)
public class UserController {
    Date date=new Date();
    SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private UserBiz userBiz;

    @RequestMapping(value = "/login")
    @ResponseBody
    public User login(String uname, String upwd, HttpSession session){
        User user=userBiz.login(uname,uname,upwd);
        if (user!=null){
            int userid=user.getUserid();
            session.setAttribute("userid",userid);
            session.setAttribute("user",user);
        }
        return user;
    }


    @RequestMapping(value = "/register")
    @ResponseBody
    public boolean register(String uname,String upwd){
        User user=new User(uname,upwd,format.format(date));
        boolean bo=userBiz.register(user);
        if(bo){
            return true;
        }else{
            return false;
        }
    }
    @RequestMapping(value = "/loginout")
    @ResponseBody
    public boolean loginout(HttpSession session){
        session.removeAttribute("userid");
        session.removeAttribute("user");
        return true;
    }

    @RequestMapping(value = "/ExitsName")
    @ResponseBody
    public boolean ExitsName(String uname) {
        User user=userBiz.ExitsName(uname);
        if (user==null){
            return false;
        }else {
        return true;
        }
    }

    @RequestMapping(value = "/findId")
    @ResponseBody
    public User  findId(int userid){
        return userBiz.findId(userid);
    }

    @RequestMapping(value = "/updateadress")
    @ResponseBody
    public boolean updateadress(int userid,String uadress){
        return userBiz.updateadress(userid,uadress);
    }


    //修改用户
    @RequestMapping(value = "/updateUser")
    @ResponseBody
    public boolean updateUser(String uname,String usex,String utel,String uadress,String upath,HttpSession session){
      int id=(int)session.getAttribute("userid");
      User user=(User)session.getAttribute("user");
      User user1=new User(id,uname,user.getUpwd(),user.getUtype(),usex,utel,uadress,upath);
      return userBiz.updateUser(user1);
    }

    //修改密码
    @RequestMapping(value = "/updatePassword")
    @ResponseBody
    public boolean updatePassword(String initial,String newpassword,HttpSession session){
       User user=(User)session.getAttribute("user");
       if (initial.equals(user.getUpwd())){
           return userBiz.updatePassword(user.getUserid(),newpassword);
       }else{
           return false;
       }
    }
    @RequestMapping(value = "/findUser")
    @ResponseBody
    public List<User> findUser() {
        return userBiz.findUser();
    }

    @RequestMapping(value = "/found")
    @ResponseBody
    public List<User> found(String uname,String utel){
        return userBiz.found(uname,utel);
    }
}
