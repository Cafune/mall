package com.liu.mall.controller;

import com.liu.mall.biz.SpecsBiz;
import com.liu.mall.entity.Specs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("specs")
@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)
public class SpecsController {
    @Autowired
    private SpecsBiz specsBiz;

    @RequestMapping(value = "/findAll")
    @ResponseBody
    public List<String> findAll(){
        List<Specs> list=specsBiz.findAll();
        List<String> list1=new ArrayList<>();
        for (int i=0;i<list.size();i++){
            list1.add(list.get(i).getStype());
        }
        return list1;
    }


}
