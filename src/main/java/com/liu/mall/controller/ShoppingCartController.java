package com.liu.mall.controller;

import com.liu.mall.biz.ProductBiz;
import com.liu.mall.biz.ShoppingCartBiz;
import com.liu.mall.entity.ShoppingCart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("shoppingcart")
@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)
public class ShoppingCartController {
    @Autowired
    private ShoppingCartBiz shoppingCartBiz;
    @Autowired
    private ProductBiz productBiz;

    @RequestMapping(value = "/findCart")
    @ResponseBody
    public Map<String,Object> findCart(int userid){
        List<ShoppingCart> list= shoppingCartBiz.findCart(userid);
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("list",list);
        return map;
    }

    @RequestMapping(value = "/addCart")
    @ResponseBody
    public boolean addCart(ShoppingCart shoppingCart){
        return shoppingCartBiz.addCart(shoppingCart);
    }

    @RequestMapping(value = "/delCart")
    @ResponseBody
    public boolean delCart(int cid){
        return shoppingCartBiz.delCart(cid);
    }
}
