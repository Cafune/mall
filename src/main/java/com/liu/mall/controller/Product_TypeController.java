package com.liu.mall.controller;

import com.liu.mall.biz.Product_TypeBiz;
import com.liu.mall.entity.Product_Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("product_type")
@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)
public class Product_TypeController {
    @Autowired
    private Product_TypeBiz product_typeBiz;

    @RequestMapping(value = "/findName")
    @ResponseBody
    public List<String> findName(ModelMap map){
        List<Product_Type> list=product_typeBiz.findName();
        List<String> list1=new ArrayList<>();
        for (int i=0;i<list.size();i++){
            list1.add(list.get(i).getType_name());
        }
        return list1;
    }

}
