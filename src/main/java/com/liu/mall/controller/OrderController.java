package com.liu.mall.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.liu.mall.biz.OrderBiz;

import com.liu.mall.config.AlipayConfig;
import com.liu.mall.entity.Order;
import com.liu.mall.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Controller
@RequestMapping("order")
@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)
public class OrderController {
    Random rand=new Random();
    Date date=new Date();
    SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @Autowired
    private OrderBiz orderBiz;

    @RequestMapping(value = "/findMy")
    @ResponseBody
    public List<Order> findMy(HttpSession session){
        int userid=(int)session.getAttribute("userid");
        List<Order> list=orderBiz.findMy(userid);
        return  list;
    }


    @RequestMapping(value = "/found")
    @ResponseBody
    public List<Order> found(String ono,String uname,String ostate,String otype){

        return orderBiz.found(ono,uname,ostate,otype);
    }


    @RequestMapping(value = "/findId")
    @ResponseBody
    public Order findId(int orid){
       Order list=orderBiz.findId(orid);
        return  list;
    }

    //退货
    @RequestMapping(value = "/updateReturn")
    @ResponseBody
    public boolean updateReturn(int orid,String returnreason) {
        return orderBiz.updateReturn(orid,returnreason);
    }

    //退款
    @RequestMapping(value = "/updateReturn1")
    @ResponseBody
    public boolean updateReturn1(int orid) {
        return orderBiz.updateReturn1(orid);
    }

    //已退款
    @RequestMapping(value = "/returnmoney")
    @ResponseBody
    public boolean  returnmoney( int orid){
        return orderBiz.returnmoney(orid);
    }
    //已退货
    @RequestMapping(value = "/updateFinishReturn")
    @ResponseBody
    public boolean updateFinishReturn(int orid){
        return  orderBiz.updateFinishReturn(orid);
    }
    @RequestMapping(value = "/findNopay")
    @ResponseBody
    public List<Order> findNopay(HttpSession session ){
        User user=(User)session.getAttribute("user");
        return orderBiz.findNopay(user.getUserid());
    }

    @RequestMapping(value = "/findOrder")
    @ResponseBody
    public List<Order> findOrder(){
        List<Order> list=orderBiz.findOrder();
        return  list;
    }

    @RequestMapping(value = "/findtui")
    @ResponseBody
    public List<Order> findtui(){
        return orderBiz.findtui();
    }
    @RequestMapping(value = "/jiesuan")
    @ResponseBody
    public boolean jiesuan(int orid){
        return orderBiz.jiesuan(orid);
    }
    @RequestMapping(value = "/findNoSend")
    @ResponseBody
    public List<Order> findNoSend(HttpSession session){
        User user=(User)session.getAttribute("user");
        return orderBiz.findNoSend(user.getUserid());
    }

    @RequestMapping(value = "/finishOrder")
    @ResponseBody
    public boolean finishOrder(String ono){
        return orderBiz.finishOrder(ono);
    }

    @RequestMapping(value = "/findSend")
    @ResponseBody
    public List<Order> findSend(HttpSession session){
       User user=(User)session.getAttribute("user");
        return orderBiz.findSend(user.getUserid());
    }

    @RequestMapping(value = "/findConfirm")
    @ResponseBody
    public boolean findConfirm(int orid){
        return  orderBiz.findConfirm(orid);
    }

    @RequestMapping(value = "/findConfirmSend")
    @ResponseBody
    public boolean findConfirmSend(int orid){
        return  orderBiz.findConfirmSend(orid);
    }

    @RequestMapping(value = "/addOrder")
    @ResponseBody
    public boolean addOrder(int pid,String pspecs,int cnum,String ctotal,HttpSession session){
    User user=(User)session.getAttribute("user");
    Order order=new Order(String.valueOf(rand.nextInt(1000)+287673015),format.format(date),user.getUserid(),user.getUname()
    ,user.getUtel(),user.getUadress(),pid,pspecs,cnum,ctotal," ","未支付"," ");
    return orderBiz.addOrder(order);
    }

    @RequestMapping(value = "/addOrder1")
    @ResponseBody
    public boolean addOrder1(int pid,String pspecs,int cnum,String ctotal,HttpSession session){
        User user=(User)session.getAttribute("user");
        Order order=new Order(String.valueOf(rand.nextInt(1000)+287673015),format.format(date),user.getUserid(),user.getUname()
                ,user.getUtel(),user.getUadress(),pid,pspecs,cnum,ctotal,"支付宝","待发货"," ");
        return orderBiz.addOrder(order);
    }


    @RequestMapping(value = "/delOrder")
    @ResponseBody
    public boolean delOrder(int orid){
        return orderBiz.delOrder(orid);
    }

    //查找全部订单个数
     @RequestMapping(value ="/findCount")
     @ResponseBody
     public int findCount(){
        return orderBiz.findCount();
     }

     //修改物流
    @RequestMapping(value ="/updatelogistics")
    @ResponseBody
    public boolean updatelogistics(int orid, String logistics) {
        return orderBiz.updatelogistics(orid,logistics);
    }
    //查找所有未支付订单个数
    @RequestMapping(value ="/findNoPayCount")
    @ResponseBody
    public int findNoPayCount(){
        return orderBiz.findNoPayCount();
    }
    //查找所有待发货订单个数
    @RequestMapping(value ="/findNoSendCount")
    @ResponseBody
    public int findNoSendCount(){
        return orderBiz.findNoSendCount();
    }
    //查找所有已发货订单个数
    @RequestMapping(value ="/findSendCount")
    @ResponseBody
    public int findSendCount(){
        return orderBiz.findSendCount();
    }
    //查找所有待确认收获订单个数
    @RequestMapping(value ="/findWaitSendCount")
    @ResponseBody
    public int findWaitSendCount(){
        return orderBiz.findWaitSendCount();
    }
    //查找所有已完成订单个数
    @RequestMapping(value ="/findFinishCount")
    @ResponseBody
    public int findFinishCount(){
        return orderBiz.findFinishCount();
    }
    //查找所有待退货订单个数
    @RequestMapping(value ="/findNoDealCount")
    @ResponseBody
    public int findNoDealCount(){
        return orderBiz.findNoDealCount();
    }
    //查找所有已退款订单个数
    @RequestMapping(value ="/findDealCount")
    @ResponseBody
    public int findDealCount(){
        return  orderBiz.findDealCount();
    }

    @RequestMapping(value ="/isbuy")
    @ResponseBody
    public int isbuy(int pid,HttpSession session){
        User user=(User)session.getAttribute("user");
        return  orderBiz.isbuy(user.getUserid(),pid);
    }

    //支付宝沙箱支付
    @GetMapping("/alipay")
    public void alipay(String cname, int totalprice,HttpServletRequest request, HttpServletResponse response) throws AlipayApiException, IOException {
        //System.out.println("获取:"+cname+" "+totalprice);
        //根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.URL
                , AlipayConfig.APPID, AlipayConfig.RSA_PRIVATE_KEY, "json",
                AlipayConfig.charset, AlipayConfig.ALIPAY_PUBLIC_KEY, AlipayConfig.sign_type);
        AlipayTradePagePayRequest requestali = new AlipayTradePagePayRequest();
        requestali.setReturnUrl(AlipayConfig.return_url);
        requestali.setNotifyUrl(AlipayConfig.notify_url);
        //通知数据中的out_trade_no为商户系统中创建的订单号
        String out_trade_no = UUID.randomUUID().toString();
        requestali.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\","
                + "\"total_amount\":\"" + totalprice + "\","
                + "\"subject\":\"" + "织里销售平台" + "\","
                + "\"body\":\"" + cname + "\","
                + "\"timeout_express\":\"100m\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        //请求
        String result = "";
        try {
            result = alipayClient.pageExecute(requestali).getBody();
            //System.out.println(result.toString());
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }

        //输出
        response.setContentType("text/html;charset="+AlipayConfig.charset);
        response.getWriter().write(result);// 直接将完整的表单html输出到页面
        response.getWriter().flush();
        response.getWriter().close();
    }

}
