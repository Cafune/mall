package com.liu.mall.controller;

import com.liu.mall.biz.ReviewBiz;
import com.liu.mall.entity.Review;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("review")
@CrossOrigin(origins = "http://localhost:8080",maxAge = 3600)
public class ReviewController {
    Date date=new Date();
    SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private ReviewBiz reviewBiz;

    @RequestMapping(value = "/addReview")
    @ResponseBody
    public boolean addReview(int pid, String pspecs, String ono, int starlevel, String preview, HttpSession session){
     int userid=(int)session.getAttribute("userid");
     Review review=new Review(userid,pid,pspecs,ono,format.format(date),starlevel,preview);
     return  reviewBiz.addReview(review);
    }

    @RequestMapping(value = "/findReview")
    @ResponseBody
    public List<Review> findReview(int pid){
        return reviewBiz.findReview(pid);
    }

    @RequestMapping(value = "/findMy")
    @ResponseBody
    public List<Review> findMy(int userid){
        return reviewBiz.findMy(userid);
    }
}
