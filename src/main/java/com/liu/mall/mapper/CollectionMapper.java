package com.liu.mall.mapper;

import com.liu.mall.entity.Collection;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CollectionMapper {
    //加入收藏
    public boolean addCollection(Collection collection);
    //查看我的收藏
    public List<Collection> findMy(int userid);
    //是否收藏
    public Collection isCollection(@Param("userid")int userid,@Param("pid")int pid);
    //取消收藏
    public boolean delCollection(@Param("userid")int userid,@Param("pid")int pid);
}
