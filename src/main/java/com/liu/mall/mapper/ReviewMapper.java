package com.liu.mall.mapper;

import com.liu.mall.entity.Review;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ReviewMapper {
    public boolean addReview(Review review);
    //对应商品的的评价
    public List<Review> findReview(int pid);
    //查看我的评价
    public List<Review> findMy(int userid);
}
