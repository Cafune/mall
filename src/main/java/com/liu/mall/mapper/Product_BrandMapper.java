package com.liu.mall.mapper;

import com.liu.mall.entity.Product_Brand;
import com.liu.mall.entity.Product_Type;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface Product_BrandMapper {
    public List<Product_Brand> findName();
}
