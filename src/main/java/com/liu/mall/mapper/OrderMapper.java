package com.liu.mall.mapper;

import com.liu.mall.entity.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMapper {
    //添加订单
    public boolean addOrder(Order order);
    //查找我的订单
    public List<Order> findMy(int userid);
    //查找订单id
    public Order findId(int orid);
    //查找所有订单
    public List<Order> findOrder();
    //查找待付款的订单
    public List<Order> findNopay(int userid);
    //查找已退款的订单
    public List<Order> findtui();
   //结算的方法
    public boolean jiesuan(int orid);
    //查找待发货的订单
    public List<Order> findNoSend(int userid);
    //查找待收货的订单
    public List<Order> findSend(int userid);
    //是否购买过
    public int isbuy(@Param("userid")int userid,@Param("pid")int pid);
    //完成评价修改订单状态
    public boolean finishOrder(String ono);

    //修改物流
    public boolean updatelogistics(@Param("orid") int orid, @Param("logistics") String logistics);

    //待退货
    public boolean updateReturn(@Param("orid") int orid,@Param("returnreason") String returnreason);

    //待退款
    public boolean updateReturn1( int orid);

    //已退款
    public boolean  returnmoney( int orid);
    //已退货
    public boolean updateFinishReturn(int orid);
    //确认收货
    public boolean findConfirm(int orid);

    //确认发货
    public boolean findConfirmSend(int orid);
    //取消订单
    public boolean delOrder(int orid);

    //查找全部订单个数
    public int findCount();
    //查找所有未支付订单个数
    public int findNoPayCount();
    //查找所有待发货订单个数
    public int findNoSendCount();
    //查找所有已发货订单个数
    public int findSendCount();
    //查找所有待确认收获订单个数
    public int findWaitSendCount();
    //查找所有已完成订单个数
    public int findFinishCount();
    //查找所有待退货订单个数
    public int findNoDealCount();
    //查找所有已退款订单个数
    public int findDealCount();

    public List<Order> found(@Param("ono")String ono,@Param("uname")String uname,@Param("ostate")String ostate,@Param("otype")String otype);
}
