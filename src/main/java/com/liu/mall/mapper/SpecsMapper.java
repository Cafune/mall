package com.liu.mall.mapper;

import com.liu.mall.entity.Specs;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SpecsMapper {
    public List<Specs> findAll();

}
