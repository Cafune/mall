package com.liu.mall.mapper;

import com.liu.mall.entity.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProductMapper {
    //查询所有商品
    public List<Product> findAllProduct();
    //查询类型
    public List<Product> findType(String ptype);
    //查询品牌
    public List<Product> findBrand(String pbrand);
    //查询名字
    public List<Product> findName(@Param("pname") String pname,@Param("ptype") String ptype);
    //查询前十个商品为新品
    public List<Product> findNew(Map<String,Object> map);
    //查询商品id
    public Product findId(int pid);
    //修改库存
    public boolean updateStock(@Param("pstock") int pstock,@Param("pid") int pid);
    //编辑商品
    public boolean updateProduct(Product product);
    //删除商品
    public boolean delProduct( int pid);
    //添加商品
    public boolean addProduct(Product product);

    public int productCount();
}
