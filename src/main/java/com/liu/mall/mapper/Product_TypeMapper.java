package com.liu.mall.mapper;

import com.liu.mall.entity.Product_Type;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface Product_TypeMapper {
    public List<Product_Type> findName();
}
