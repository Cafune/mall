package com.liu.mall.mapper;

import com.liu.mall.entity.Reason;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ReasonMapper {
    public List<Reason> findReason();
    public boolean updateReason(@Param("reid") int reid, @Param("rreason") String rreason);
    public boolean delReason(int reid);
    public boolean addReason(Reason reason);
    public  List<Reason> findReason1(@Param("rreason") String rreason);
}
