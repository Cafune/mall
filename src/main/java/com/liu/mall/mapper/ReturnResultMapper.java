package com.liu.mall.mapper;

import com.liu.mall.entity.ReturnResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ReturnResultMapper {
    public boolean addReturn(ReturnResult ReturnResult);
    public List<ReturnResult> findAll();
    public boolean isReturn(@Param("rsid") int rsid, @Param("aname") String aname,@Param("returntime") String returntime);
}
