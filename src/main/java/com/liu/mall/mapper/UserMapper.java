package com.liu.mall.mapper;

import com.liu.mall.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {
    //用户登录
    public User login(@Param("uname")String uname,@Param("utel")String utel, @Param("upwd")String upwd);
    //用户注册
    public boolean register(User user);
    //是否同名
    public User ExitsName(String uname);
    //根据id查找用户
    public User findId(int userid);
    //修改地址
    public boolean updateadress(@Param("userid") int userid,@Param("uadress") String uadress);
    //修改用户
    public boolean updateUser(User user);
    //修改密码
    public boolean updatePassword(@Param("userid")int userid,@Param("upwd")String upwd);
    public List<User> findUser();

    public List<User> found(@Param("uname") String uname,@Param("utel")String utel);
}
