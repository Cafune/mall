package com.liu.mall.mapper;

import com.liu.mall.entity.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ShoppingCartMapper {
    //查找所有购物车
    public List<ShoppingCart> findCart(int userid);
    //增加购物车信息
    public boolean addCart(ShoppingCart shoppingCart);
    //删除购物车信息
    public boolean delCart(int cid);
}
